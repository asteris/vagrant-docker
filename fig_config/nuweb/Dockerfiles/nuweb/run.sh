#!/bin/bash

/run_test.sh

source /etc/apache2/envvars

export APACHE_LOG_DIR=/logs/httpd
mkdir -p ${APACHE_LOG_DIR}

echo "Starting Apache" 
exec apache2 -D FOREGROUND
