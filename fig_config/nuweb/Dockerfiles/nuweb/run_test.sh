#!/bin/bash

BASEDIR=/www/sites/web/fuel/packages/rift/lib

cd ${BASEDIR} && composer install 

if [ -z ${SKIP_TESTS+x} ]; then
  FUEL_ENV=Test ${BASEDIR}/vendor/bin/codecept run
fi 

