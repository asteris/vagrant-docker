#!/bin/bash

set -x 
#Start the container config in fig.yml 
export FIG=/usr/local/bin/fig
export FIG_OPTS=" -d "

export FIG_PROJECT_NAME=nuweb

export CONFIG_DIR=/fig_config/${FIG_PROJECT_NAME}

#Start the container environment
cd ${CONFIG_DIR} && ${FIG} -f ${CONFIG_DIR}/fig.yml up ${FIG_OPTS} 

