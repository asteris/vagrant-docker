#!/bin/bash

#Allow the default sudo user to be able to run docker
#commands without being root

echo "Adding vagrant to docker group"
usermod -aG docker vagrant
