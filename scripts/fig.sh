#!/bin/bash

export FIG=/usr/local/bin/fig
export FIG_VERSION=1.0.0

if [  -f ${FIG} ]; then
  echo "fig already installed at ${FIG}"
else 
  echo "Installing fig ${FIG_VERSION}"
  curl --silent -L https://github.com/docker/fig/releases/download/${FIG_VERSION}/fig-Linux-x86_64 > ${FIG}
  chmod +x  ${FIG}
fi

