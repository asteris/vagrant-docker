#!/bin/bash

set -x 
set -e 

#This is an example script of restoring mongo data 
# The mongoRestore in Nuweb/fig.yml performs the exact
# same functionality. 

#What is required: 
#A running MongoDB container
#Mongo back up files located in /files/dump on the host

export MONGO_CONTAINER=NUweb_mongo_1
export MONGO_LINK_HOSTNAME=mongo
export MONGO_TAG=2.2  #container tag

sleep 5 #wait for mongo to come up
docker run --rm --v /files/dump:/files/dump:ro \
           --link ${MONGO_CONTAINER}:${MONGO_LINK_HOSTNAME} \
           mongo:${MONGO_TAG} \
           mongorestore --host ${MONGO_LINK_HOSTNAME}  /files/dump/
